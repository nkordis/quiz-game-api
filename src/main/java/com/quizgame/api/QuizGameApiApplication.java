package com.quizgame.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuizGameApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuizGameApiApplication.class, args);
	}

}
