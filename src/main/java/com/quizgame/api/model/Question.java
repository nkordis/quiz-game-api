package com.quizgame.api.model;

import java.util.List;

public class Question {
	
	private enum Category {}
	private enum Type{}
	private enum Difficulty{}
	
	private Integer id;
	private Category category;
	private Type type;
	private Difficulty difficulty;
	private String question;
	private String correct_answer;
	private List<String> incorrect_answers;
	
	
	public Question() {
		super();
	}


	public Question(Integer id, Category category, Type type, Difficulty difficulty, String question,
			String correct_answer, List<String> incorrect_answers) {
		super();
		this.id = id;
		this.category = category;
		this.type = type;
		this.difficulty = difficulty;
		this.question = question;
		this.correct_answer = correct_answer;
		this.incorrect_answers = incorrect_answers;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}


	public Type getType() {
		return type;
	}


	public void setType(Type type) {
		this.type = type;
	}


	public Difficulty getDifficulty() {
		return difficulty;
	}


	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}


	public String getQuestion() {
		return question;
	}


	public void setQuestion(String question) {
		this.question = question;
	}


	public String getCorrect_answer() {
		return correct_answer;
	}


	public void setCorrect_answer(String correct_answer) {
		this.correct_answer = correct_answer;
	}


	public List<String> getIncorrect_answers() {
		return incorrect_answers;
	}


	public void setIncorrect_answers(List<String> incorrect_answers) {
		this.incorrect_answers = incorrect_answers;
	}


	@Override
	public String toString() {
		return "Question [id=" + id + ", category=" + category + ", type=" + type + ", difficulty=" + difficulty
				+ ", question=" + question + ", correct_answer=" + correct_answer + ", incorrect_answers="
				+ incorrect_answers + "]";
	}
	
	
}
